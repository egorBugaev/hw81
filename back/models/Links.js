const mongoose = require('mongoose');

const LinksSchema = new mongoose.Schema({
  baseURL: {
    type: String, required: true
  },
  shortURL: {
    type: String, required: true
  }
});

const Links = mongoose.model('Links', LinksSchema);

module.exports = Links;