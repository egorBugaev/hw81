const express = require('express');

const nanoid = require('nanoid');
const Links = require('../models/Links');



const router = express.Router();


const createRouter = () => {

    router.get('/', (req, res) => {
        Links.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

   router.post('/', (req, res) => {
    const baseLink = req.body;
    baseLink.shortURL='http://localhost:8000/' + nanoid(6);
       //   Согласно документации для наших целей проверка уникальности не требуется
       //https://alex7kom.github.io/nano-nanoid-cc/
       const link = new Links(baseLink);

    link.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:shortURL', (req, res) => {
    Links.find().findOne({shortURL: 'http://localhost:8000/' + req.params.shortURL})
        .then(result =>  res.status(301).redirect(result.baseURL))
        .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;