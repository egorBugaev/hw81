import React, { Component } from 'react';
import './App.css';
import {getLinks, goToLink, postLink} from "./store/actions";
import {connect} from "react-redux";

class App extends Component {

    state = {
        baseURL: '',
    };

    componentDidMount(){
        this.props.get();
    }

    postToDb = event => {
        event.preventDefault();
        this.props.post(this.state).then(response => {
            if(!response.error) {
                this.setState({
                    baseURL:''
                });
            }
        }).then(this.props.get);
    };

    inputChangeHandler = event => {
        event.persist();
        this.setState({
            [event.target.name]: event.target.value
        });
    };



    render() {
      let shortedLink =null;
      if(this.props.link){
          shortedLink=<div>
              <h3>this your short link</h3>
                  {   this.props.link.map(item => {
                  return <li
                  key={item._id}
                  className="this-item">
                  <a href={item.baseURL}>{item.shortURL}</a>

                  </li>
              })
                  }

          </div>
      }
    return (
      <div className="App">
       <h2>Link to short</h2>
          <input required value={this.state.baseURL} onChange={this.inputChangeHandler} name="baseURL" type="text"/>
          <button onClick={this.postToDb}>To short</button>
          {shortedLink}
      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
        link: state.links,
        shortURL: state.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        get: () => dispatch(getLinks()),
        getNewsByID: (id) => dispatch(goToLink(id)),
        post: (id) => dispatch(postLink(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

