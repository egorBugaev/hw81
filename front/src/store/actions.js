import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS, FETCH_POST_ERROR} from "./actionTypes";
import axios from '../axios-api';

export const fetchPostSuccess = links => {
	return {type: FETCH_POST_SUCCESS, links};
};

export const fetchGetSuccess = links => {
	return {type: FETCH_GET_SUCCESS, links};
};

export const fetchGetSuccessLink = links => {
	return {type: FETCH_GET_SUCCESS, links};
};

export const fetchPostError = error => {
    return {type: FETCH_POST_ERROR, error}
};

export const postLink = data => {
	return dispatch => {
		return axios.post('/links', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const getLinks = () => {
	return dispatch => {
		return axios.get('/links').then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};

export const goToLink = id => {
	return dispatch => {
		return axios.get(`/links/${id}`).then(response => dispatch(fetchGetSuccessLink(response.data)));
	}
};
