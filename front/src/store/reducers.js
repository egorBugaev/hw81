import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS} from "./actionTypes";

const initialState = {
	links: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POST_SUCCESS:
			return {...state, links: action.link};
		case FETCH_GET_SUCCESS:
            console.log(action.links);
            return {...state, links: action.links};
		default:
			return state;
	}
};

export default reducer;
